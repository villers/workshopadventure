#!/bin/bash
set -e


DIR=$(dirname "$0")
pushd $DIR

echo "This script create a kubeconfig file for local runner in kubeconfig.local"

set -x
kubectl konfig export $(kubectl config current-context)| sed "s/server:.*/server: https:\/\/kubernetes.default:443/g" > kubeconfig.local
{ set +x; } 2> /dev/null # silently disable xtrace

echo ""
echo "set var KUBECONFIG_FILE with this on gitlab group or on the repository containing the deployments descriptors"
echo "🔗 - https://gitlab.com/groups/YOUR_GROUPS/-/settings/ci_cd"
echo "Or if you have only a repository containing the deployments descriptors"
echo "🔗 open https://gitlab.com/YOUR_REPOS/-/settings/ci_cd"
echo ""

cat kubeconfig.local

popd


