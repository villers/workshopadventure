# Vault (https://hub.docker.com/_/vault)
2 possibilités

## Vault in memory with token root_token on the host network
```bash
docker run --net host --cap-add=IPC_LOCK -e 'VAULT_DEV_ROOT_TOKEN_ID=root_token' -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:8200' vault:1.11.3
```
=> access via ip du host 

## Vault in memory with token root_token on the cluster network 
```
docker network ls
```
=> port le nom du cluster, ici k3d-local

```
docker run --rm --net k3d-local --name vault --cap-add=IPC_LOCK -e 'VAULT_DEV_ROOT_TOKEN_ID=root_token' -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:8200' vault:1.11.3
```
=> vault sera accessible http://vault:8200 depuis les conteneurs (donc le control plane) FAUT => pas résolution DNS... TODO voir pour le faire tourner dans le cluster

le faire tourner sur la machine avec le host network
```
docker run --rm -d --net host --name vault --cap-add=IPC_LOCK -e 'VAULT_DEV_ROOT_TOKEN_ID=root_token' -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:8200' vault:1.11.3
```

# Secrets Store CSI Driver
Lire aussi https://arslan.io/2018/06/21/how-to-write-a-container-storage-interface-csi-plugin/


https://secrets-store-csi-driver.sigs.k8s.io/getting-started/installation.html

```bash
helm repo add secrets-store-csi-driver https://kubernetes-sigs.github.io/secrets-store-csi-driver/charts
helm install csi-secrets-store secrets-store-csi-driver/secrets-store-csi-driver --namespace kube-system
```

OU de puis le dossier vault 

```bash
kubectl apply -f csi-driver/deploy/rbac-secretproviderclass.yaml -n kube-system
kubectl apply -f csi-driver/deploy/csidriver.yaml -n kube-system
kubectl apply -f csi-driver/deploy/secrets-store.csi.x-k8s.io_secretproviderclasses.yaml -n kube-system
kubectl apply -f csi-driver/deploy/secrets-store.csi.x-k8s.io_secretproviderclasspodstatuses.yaml -n kube-system
kubectl apply -f csi-driver/deploy/secrets-store-csi-driver.yaml -n kube-system

# If using the driver to sync secrets-store content as Kubernetes Secrets, deploy the additional RBAC permissions
# required to enable this feature
kubectl apply -f deploy/rbac-secretprovidersyncing.yaml
```

Note : avec les taints pas de csi node sur le controlplane, à voir il faut ajouter une tolération ou si pas utile


# Vault External Secret Providers

https://github.com/hashicorp/vault-csi-provider

```bash
kubectl apply -f vault/deploy/vault-csi-provider.yaml
```

### Version interne de vault

```
## Add vault repo
helm repo add hashicorp https://helm.releases.hashicorp.com
helm repo update

## Install vault without csi (already done)
helm install vault hashicorp/vault \
    --set "server.dev.enabled=true" \
    --set "injector.enabled=false" \
    --set "csi.enabled=false" \
    --create-namespace -n vault \
    --wait
```


# Utilisation (dans kube)

```
kubectl exec -it vault-0 -n vault -- /bin/sh

# dans le pod

## ajout d'un secret de test
vault kv put secret/james-secret secret="Hello DevFest Nantes"

# test d'affichage
vault kv get secret/james-secret


## configuration de l'auth kube (pour que sa du csi puisse se connecter à vault)

# active auth kube
vault auth enable kubernetes

# configure la chaine de connection (pourrait utiliser $KUBERNETES_PORT_443_TCP_ADDR pour l'ip mais...)
vault write auth/kubernetes/config \
    kubernetes_host="https://kubernetes.default:443"


# créer une policy avec le droit de lecture sur le path james-secret
# TODO check secret/data/james-secret/*
vault policy write internal-app - <<EOF
path "secret/data/james-secret" {
  capabilities = ["read"]
}
EOF

# créer un role james-reader qui à la policy internal-app au SA kube de l'app qui va consommer le secret
vault write auth/kubernetes/role/james-reader \
    bound_service_account_names=webapp-sa \
    bound_service_account_namespaces=default \
    policies=internal-app \
    ttl=20m
```

# Definition de la ressource pour le csi 
```yaml
apiVersion: secrets-store.csi.x-k8s.io/v1
kind: SecretProviderClass
metadata:
  name: vault-database
spec:
  provider: vault
  parameters:
    vaultAddress: "http://vault.vault:8200"
    roleName: "james-reader"
    objects: |
      - objectName: "q"
        secretPath: "secret/data/james-secret"
        secretKey: "secret"
```        


KO

---
TEST https://www.hashicorp.com/blog/retrieve-hashicorp-vault-secrets-with-kubernetes-csi
Ca marche !!!





### version externe de vault

## Connexion à vault
```
docker exec -ti -e VAULT_ADDR='http://127.0.0.1:8200' -e VAULT_TOKEN='root_token' vault /bin/sh
```
## Configuration vault

L'authentification à vault est faite en utilisant le token kube du service account

### 1. Récuperer l'url de l'api srv (en externe, en interne on utilise le svc)
```shell
kubectl cluster-info                          
```

Renvoi ici
```
Kubernetes control plane is running at https://0.0.0.0:37909
...
```
=> 127.0.0.1:37909

### Création d'un user avec des droits de token_reviewer 

https://www.vaultproject.io/api-docs/auth/kubernetes#token_reviewer_jwt

https://learn.hashicorp.com/tutorials/vault/agent-kubernetes

https://www.vaultproject.io/docs/auth/kubernetes#discovering-the-service-account-issuer

Création du compte permettant de valider les tokens (qui sera utilisé par vault) et récupération des infos
```
kubectl apply -f vault-jwt-sa.yaml
# le ca.crt : kubernetes_ca_cert
kubectl -n kube-system get secret vault-auth-secret -o jsonpath='{.data.ca\.crt}' | base64 -d
# le token : token_reviewer_jwt
kubectl -n kube-system get secret vault-auth-secret -o jsonpath='{.data.token}' | base64 -d
```
On récupère la ca.crt et le token

### 2. Configurer l'authent


Ajout d'une policy permettant de lire l'arbo `secret/data/myapp/*` (qui sera utilisé pour stocker les secrets pour kube)
```
vault policy write myapp-kv-ro - <<EOF
path "secret/data/myapp/*" {
    capabilities = ["read", "list"]
}
EOF
```

Création d'un secret de test dans cette arbo

```shell
vault kv put secret/myapp/config \
      username='appuser' \
      password='suP3rsec(et!' \
      ttl='30s'
```

Avec les valeurs récupéré précedement (cf vault-jwt-sa.yaml)  

> token_reviewer_jwt = token d'un compte pouvant valider les token des clients (créer précedement)
> kubernetes_ca_cert = pem encoded ca
```shell
# activer l'authent (avec des valeurs d'exemple, voir les commandes précedentes pour automatiser cela)
vault auth enable kubernetes
# 
vault write auth/kubernetes/config \
    token_reviewer_jwt="eyJhbGciOiJSUzI1NiIsImtpZCI6IldYenVYRnZNT0xpd2ZRSHYxUTVGMUFZS1hjMFYyOVJoN1VDNTJrTVRYV1EifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6InZhdWx0LWF1dGgtc2VjcmV0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6InZhdWx0LWF1dGgiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJiZWM3MjY3OC0zYzBhLTRiNzgtYTdhNy1hMTczYzNhNTcwOWYiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6ZGVmYXVsdDp2YXVsdC1hdXRoIn0.b1OCk7OxhSq5Y__nNHHZkZ5xKYApSbwenocXAi1mk20vgISk0LCVlBmNc8KC07xM5g6m8AnlKz_-PAOwp7kPRqENlXdqe2Z5FP0LGtfbpRBnRAPgmiza47O18fvVjpopyWNxPwFSy8_6spbmuRe6rwGB1qJKiWuAvt88rKOb-8X67VgSuVOCCMRTAdf4-fmKxa9Jj70q8nSpxclu7axz3SlyAbIHRpSAVEgIlH-z1xKAuPPIqyxKDYcOvEoCfu0nAgCVTkw5aXO7tLr0Yn0POaUpCzB-FbGkXwLwUuYYGvc9HshM_PyOZlQpw5_Pev_xrE8vPSbnL14eosEEWZun7w" \
    kubernetes_host="https://127.0.0.1:37909" \
    kubernetes_ca_cert="-----BEGIN CERTIFICATE-----
MIIBeDCCAR2gAwIBAgIBADAKBggqhkjOPQQDAjAjMSEwHwYDVQQDDBhrM3Mtc2Vy
dmVyLWNhQDE2NjU0MDAyNzgwHhcNMjIxMDEwMTExMTE4WhcNMzIxMDA3MTExMTE4
WjAjMSEwHwYDVQQDDBhrM3Mtc2VydmVyLWNhQDE2NjU0MDAyNzgwWTATBgcqhkjO
PQIBBggqhkjOPQMBBwNCAAQUc7JDGOTqgYO7nWSCqsfowK2CzW0BtX8o8oJhHqrq
rIfE6zft7978l0CAU5jJ6yzO9NthK4ZVO/hpIxQnYqWdo0IwQDAOBgNVHQ8BAf8E
BAMCAqQwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUF4KnC0aeUobzoWTfuTH/
01HHTeYwCgYIKoZIzj0EAwIDSQAwRgIhAJoOe9Sf7+IbZYioMxopbSPBQJMJgmOp
pK1M558ar7sQAiEAu/C7Va3wCifLfCFfjkSvonj4Fr8IoAPYxXZGUf9Z09Y=
-----END CERTIFICATE-----
"

Attention vérife si besoin de : https://www.vaultproject.io/api-docs/auth/kubernetes#caveats

Création d\'un role "myapp" qui sera utilisé par vault pour faire le bind avec sa de kube (ici le sa est dans kube-system)

```shell
vault write auth/kubernetes/role/myapp \
     bound_service_account_names=webapp-sa \
     bound_service_account_namespaces=default \
     policies=myapp-kv-ro \
     ttl=24h
```


Autre test avec un rôle defaut qui a un paquet de droit (read sur tous les secrets)
```
printf 'path "secret/*"{\ncapabilities=["read"]\n}' | vault policy write demo-policy - &&\

vault write auth/kubernetes/role/default \
    bound_service_account_names=default \
    bound_service_account_namespaces=default \
    policies="demo-policy"
```

### 3. Configuration du csi driver pour accéder à l'arbo


Avec ici l'ip de vault  10.136.100.223
```yaml
apiVersion: secrets-store.csi.x-k8s.io/v1
kind: SecretProviderClass
metadata:
  name: vault-database
spec:
  provider: vault
  parameters:
    vaultAddress: "http://10.136.100.223:8200/:8200"
    roleName: "myapp"
    objects: |
      - objectName: "pwd"
        secretPath: "secret/myapp/config"
        secretKey: "password"
      - objectName: "user"
        secretPath: "secret/myapp/config"
        secretKey: "username"
```

c'est le b... pb de droits 403... recommencer
