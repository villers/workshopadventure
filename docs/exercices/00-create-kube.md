# Installation d'un cluster civo

## Instancier le projet de travail

**Si ce n'est pas encore fait**

Pour cette adventure votre livre de sort qui vous permettra de lancer et piloter votre infrastructure sera dans un worspace gitpod.

La première étape consiste à faire un fork du projet gitlab contenant tous les sorts et autres potions indispensables à tout aventurier du royaume de kubernetes.

- Authentifiez vous sur [gitlab](https://www.gitlab.com)
- Allez sur le projet de [livre de sort](https://gitlab.com/gitops-heros/deploy-sith-from-gitlab), faites en un [fork](https://gitlab.com/gitops-heros/deploy-sith-from-gitlab/-/forks/new) **PUBLIC** (le lien est en haut à droite)
- Ouvrez VOTRE fork du projet dans Gitpod via le bouton **Gipod** situé en haut à droite, un peu en dessous de fork (attention la combo box peut être sur "Web IDE", en ce cas changer le choix de la combo box)

Note: Si c'est la première fois que vous utilisez Gitpod vous aurez un message vous demandant de donner les droits à Gitpod:

> Enable Gitpod?  
> To use Gitpod you must first enable the feature in the integrations section of your user preferences.
> Suivez le lien pour donnez à Gitpod les droits pour ouvrir votre projet.

## Configurer la cmd civo

Une fois le projet GitPod démarré (c'est un peu plus long à la première instanciation) positionnez vous dans le shell en bas du navigateur (si non visible : cliquez en haut à gauche, puis "Terminal" > "New Terminal" OU le raccourcit clavier : "CTRL SHIFT ²" ).  
Le shell sera votre meilleur ami dans cette aventure, il vous permettra de lancer de nombreux scripts et autres sorts redoutables.  
Ouvrez votre livre de sorts en positionnant votre shell dans le répertoire `/workspace/spell-book/`

```shell
cd /workspace/spell-book/
```

Gandalf vous fournit un sortilège (aka un script) pour créer un cluster managé conforme à vos besoins dans CIVO. Pour que cela fonctionne, vous allez devoir renseigner votre APIKEY qui sera utilisé par l'outil en ligne de commande `civo`.

```shell
civo apikey save NAME <APIKEY>
```

On trouve APIKEY sur son compte [civo](https://www.civo.com/account/security) et NAME est le petit nom que vous voulez donner à cette connexion, si vous ne savez pas vous pouvez toujours l'appeler gitops.

Choisissez ensuite la région par défaut (utilisez la région LON1 et profiter des crédits généreusement offerts par CIVO pour le workshop)

```shell
civo region ls

civo region current LON1
```

## Créer le cluster

Trouvez un petit nom pour votre cluster....

```shell
export clustername=AZERTYUIOP
```

### Avec 1 seul script (option la plus simple et la plus rapide)

👀 Depuis le chapitre (aka répertoire) `civo-k3s`

```
./01-create-cluster.sh $clustername
```

## Récupérez l'IP du Load Balancer du cluster

Cela vous sera utile pour la suite pour accéder aux applications et siths déployées.  
Depuis le chapitre (répertoire) civo-k3s

```shell
source ./get-cluster-ip.sh
```

Ou si vous préférer :

```shell
export IP=$(kubectl get svc traefik-ingress-controller -n kube-system -o jsonpath='{ .status.loadBalancer.ingress[0].ip }')
echo "L'ip du load balancer est $IP"
```

Normalement un **curl** sur l'IP doit renvoyer une erreur 404 'rune not found' (à la fois sur le port 80 et le 443).
